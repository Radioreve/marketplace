const { server } = require('../lib/server')

async function start () {
  try {
    await server.start()
    console.log(`Server running at: ${server.info.uri}`)
    return server
  } catch (err) {
    console.error(err)
    process.exit(1)
  }
}

start()
