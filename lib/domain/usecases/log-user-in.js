const {
  NotFoundError
} = require('../errors')

const _ = require('lodash')

module.exports = async function logUserIn ({
  email,
  password,
  userRepository
}) {
  const found = await userRepository.findUserByEmailAndPassword({ email, password })

  if (_.isEmpty(found)) {
    throw new NotFoundError('not found')
  }
  if (_.size(found) > 1) {
    throw new Error('multiple matches')
  }
  return _.first(found)
}
