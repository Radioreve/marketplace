'use strict'

const Hapi = require('@hapi/hapi')
const routes = require('./routes')

const createServer = async () => {
  const server = Hapi.server({
    port: process.env.API_PORT,
    host: 'localhost'
  })
  const configuration = routes
  await server.register(configuration)
  return server
}
module.exports = createServer
