const users = require('../../../db/users')
const _ = require('lodash')

module.exports = {
  async findUserByEmailAndPassword ({ email, password }) {
    return _.filter(users, { email, password })
  }
}
